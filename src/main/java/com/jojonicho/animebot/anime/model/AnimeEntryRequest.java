package com.jojonicho.animebot.anime.model;

import lombok.Data;

@Data
public class AnimeEntryRequest {
    private int malId;
    private String status;
    private int rating;
}
