package com.jojonicho.animebot.anime.commands;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jojonicho.animebot.anime.model.JikanAnime;
import com.jojonicho.animebot.anime.service.JikanAnimeService;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;

public class AnimeSearchCommand extends Command {

    private final JikanAnimeService jikanAnimeService;

    /**
     * Command to search for anime from query.
     */
    public AnimeSearchCommand(JikanAnimeService jikanAnimeService) {
        this.name = "anime-search";
        // tambah singkatan
        this.aliases = new String[] {"as"};
        this.help = "search for anime from query";
        this.arguments = "<query>";
        this.botPermissions = new Permission[] {Permission.MESSAGE_EMBED_LINKS};
        this.guildOnly = false;
        this.jikanAnimeService = jikanAnimeService;
    }

    @Override
    protected void execute(CommandEvent event) {
        String[] args = event.getArgs().split("\\s+");

        JikanAnime anime = jikanAnimeService.getAnime(args);

        String title = String.format("%d - %s", anime.getMalId(), anime.getTitle());
        event.reply(new EmbedBuilder()
            .setTitle(title)
            .setImage(anime.getImageUrl())
            .setDescription(anime.getSynopsis())
            .build());
    }
}
