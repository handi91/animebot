package com.jojonicho.animebot.anime.service;

import com.jojonicho.animebot.anime.model.AnimeEntry;
import com.jojonicho.animebot.anime.repository.AnimeEntryRepository;
import com.jojonicho.animebot.discorduser.model.DiscordUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AnimeEntryServiceImpl implements AnimeEntryService {
    @Autowired
    private AnimeEntryRepository animeEntryRepository;

    @Override
    public AnimeEntry createAnimeEntry(int malId, DiscordUser discordUser) {

        AnimeEntry animeEntry = getAnimeEntry(malId, discordUser);
        if (null != animeEntry) {
            return animeEntry;
        }

        animeEntry = new AnimeEntry();
        animeEntry.setMalId(malId);
        animeEntry.setDiscordUser(discordUser);

        return animeEntryRepository.save(animeEntry);
    }

    @Override
    public Iterable<AnimeEntry> getListAnimeEntry(String userId) {
        return animeEntryRepository.findAllByDiscordUser_Id(userId);
    }

    @Override
    public AnimeEntry updateAnimeEntry(AnimeEntry animeEntry) {
        return animeEntryRepository.save(animeEntry);
    }

    @Override
    public void deleteAnimeEntry(AnimeEntry animeEntry) {
        animeEntryRepository.delete(animeEntry);
    }

    @Override
    public AnimeEntry getAnimeEntry(int malId, DiscordUser discordUser) {
        return animeEntryRepository.findByMalIdAndDiscordUser(malId, discordUser);
    }
}