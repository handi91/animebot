package com.jojonicho.animebot.anime.service;

import com.github.doomsdayrs.jikan4java.core.Retriever;
import com.jojonicho.animebot.anime.model.JikanAnime;
import com.jojonicho.animebot.anime.model.JikanAnimeList;
import java.util.concurrent.CompletableFuture;
import org.springframework.stereotype.Service;

@Service
public class JikanAnimeServiceImpl implements JikanAnimeService {

    @Override
    public JikanAnime getAnime(String[] queryArgs) {
        String url = getUrl(queryArgs);
        return get(url);
    }

    @Override
    public JikanAnime getAnime(String query) {
        String url = getUrl(query);
        return get(url);
    }

    private JikanAnime get(String url) {
        CompletableFuture<JikanAnimeList> animeSearch =
            new Retriever().retrieve(url, JikanAnimeList.class);

        JikanAnimeList list = animeSearch.join();
        return list.getResults().get(0);
    }

    private String getUrl(String[] args) {
        String query = String.join("%20", args);
        return String.format("https://api.jikan.moe/v3/search/anime?q=%s", query);
    }

    private String getUrl(String query) {
        return String.format("https://api.jikan.moe/v3/search/anime?q=%s", query);
    }
}