package com.jojonicho.animebot.schedule.commands;

import com.github.doomsdayrs.jikan4java.core.Connector;
import com.github.doomsdayrs.jikan4java.data.model.main.anime.Anime;
import com.github.doomsdayrs.jikan4java.data.model.main.schedule.Schedule;
import com.github.doomsdayrs.jikan4java.data.model.main.schedule.SubAnime;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jojonicho.animebot.anime.model.AnimeEntry;
import com.jojonicho.animebot.anime.service.AnimeEntryService;
import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;

public class ScheduleListCommand extends Command {

    private final AnimeEntryService animeEntryService;

    /**
     * Method untuk menampilkan jadwal anime berdasarkan list user.
     * @param animeEntryService Anime Entry Service
     */
    public ScheduleListCommand(AnimeEntryService animeEntryService) {
        this.name = "schedule-user-list";
        this.aliases = new String[]{"sl"};
        this.help = "get list of user list anime";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
        this.guildOnly = false;
        this.animeEntryService = animeEntryService;
    }

    @Override
    public void execute(CommandEvent event) {

        String discordUserId = event.getAuthor().getId();

        Iterable<AnimeEntry> animeEntryList = animeEntryService.getListAnimeEntry(discordUserId);

        if (!animeEntryList.iterator().hasNext()) {
            event.reactError();
            return;
        }

        CompletableFuture<Schedule> scheduleFuture = new Connector().getCurrentSchedule();
        Schedule schedule = scheduleFuture.join();
        ArrayList<ArrayList<SubAnime>> sc = new ArrayList<>();
        String[] day = {"Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"};
        sc.add(schedule.getMonday());
        sc.add(schedule.getTuesday());
        sc.add(schedule.getWednesday());
        sc.add(schedule.getThursday());
        sc.add(schedule.getFriday());
        sc.add(schedule.getSaturday());
        sc.add(schedule.getSunday());

        String title = String.format("%s's anime list", event.getAuthor().getName());
        String avatarImageUrl = event.getAuthor().getAvatarUrl();
        StringBuilder description = new StringBuilder();

        for (AnimeEntry animeEntry: animeEntryList) {
            int malId = animeEntry.getMalId();

            int count = 0;

            String eps = null;

            String hari = null;

            for (ArrayList<SubAnime> listAnime: sc) {
                count += 1;
                if (count > 6) {
                    count = 0;
                }
                for (SubAnime anime: listAnime) {
                    if (malId == anime.getMalID()) {
                        eps = String.valueOf((anime.getEpisodeCount()));
                        count--;
                        if (count == -1) {
                            count = 6;
                        }
                        hari = day[count];
                    }
                }
            }

            if (Objects.equals(hari, null)
                    && Objects.equals(eps, null)) {
                hari = " - ";
                eps = "can't be found in database";
            }

            if (Objects.equals(eps, "0")) {
                eps = "can't be found in database";
            }

            Anime idAnime = new Connector().retrieveAnime(malId).join();

            description.append(String.format("(%s) Episode %s - %d - %s\n",
                    hari, eps, malId, idAnime.getTitle()));
        }

        event.reply(new EmbedBuilder()
                .setTitle(title)
                .setDescription(description.toString())
                .setThumbnail(avatarImageUrl)
                .build()
        );
    }
}