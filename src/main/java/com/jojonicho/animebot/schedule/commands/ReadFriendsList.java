package com.jojonicho.animebot.schedule.commands;

import com.github.doomsdayrs.jikan4java.core.Connector;
import com.github.doomsdayrs.jikan4java.data.model.main.anime.Anime;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jojonicho.animebot.anime.model.AnimeEntry;
import com.jojonicho.animebot.anime.service.AnimeEntryService;
import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.discorduser.service.DiscordUserService;
import com.jojonicho.animebot.friend.service.FriendRequestService;

import java.util.Map;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;

public class ReadFriendsList extends Command {
    private final FriendRequestService friendRequestService;
    private DiscordUserService discordUserService;
    private AnimeEntryService animeEntryService;

    /**
     * Method untuk menampilkan jadwal anime berdasarkan list user.
     * @param friendRequestService Friend Anime List Service
     */
    public ReadFriendsList(FriendRequestService friendRequestService,
                           DiscordUserService discordUserService,
                           AnimeEntryService animeEntryService) {
        this.name = "friend-list-schedule";
        this.aliases = new String[]{"sfl"};
        this.help = "get anime list of user that you added or added you";
        this.arguments = "<mention name>";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
        this.guildOnly = false;
        this.friendRequestService = friendRequestService;
        this.discordUserService = discordUserService;
        this.animeEntryService = animeEntryService;

    }

    @Override
    protected void execute(CommandEvent event) {
        String[] args = event.getArgs().split("\\s+");
        String choice = args[0];
        String[] digits = choice.split("(?<=.)");
        String idFriend = "";
        for (int i = 3; i < digits.length; i++) {
            if ((i + 1) != digits.length) {
                idFriend += digits[i];
            }
        }

        String discordUserId = event.getAuthor().getId();
        DiscordUser discordUser = discordUserService.getDiscordUser(discordUserId);
        Map<String, DiscordUser> friendList;
        friendList = friendRequestService.getAllFriend(discordUser);

        for (String friendName : friendList.keySet()) {
            if (idFriend.equals(friendList.get(friendName).getId())) {
                displayUserList(event, idFriend);
            }
        }
    }

    private void displayUserList(CommandEvent event, String friendName) {
        String title = String.format("Friend anime list");
        String description = "";

        Iterable<AnimeEntry> animeEntryList = animeEntryService.getListAnimeEntry(friendName);

        if (!animeEntryList.iterator().hasNext()) {
            event.reply(new EmbedBuilder()
                    .setTitle("Anime List Not Found")
                    .build()
            );
            return;
        }

        for (AnimeEntry animeEntry : animeEntryList) {
            int malId = animeEntry.getMalId();
            Anime idAnime = new Connector().retrieveAnime(malId).join();

            description += String.format("%d - %s\n", malId, idAnime.getTitle());

        }

        event.reply(new EmbedBuilder()
                .setTitle(title)
                .setDescription(description)
                .build()
        );
    }
}