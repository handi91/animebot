package com.jojonicho.animebot.schedule.commands;

import com.github.doomsdayrs.jikan4java.core.Connector;
import com.github.doomsdayrs.jikan4java.data.model.main.schedule.Schedule;
import com.github.doomsdayrs.jikan4java.data.model.main.schedule.SubAnime;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;

public class ScheduleCommand extends Command {

    /**
     * Method untuk menampilkan jadwal anime.
     */
    public ScheduleCommand() {
        this.name = "schedule-current-weekly";
        this.aliases = new String[]{"sw"};
        this.help = "show anime/manga weekly schedule";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
        this.guildOnly = false;
    }

    @Override
    protected void execute(CommandEvent event) {

        CompletableFuture<Schedule> scheduleFuture = new Connector().getCurrentSchedule();
        Schedule schedule = scheduleFuture.join();
        ArrayList<ArrayList<SubAnime>> sc = new ArrayList<>();
        String[] day = {"Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"};
        sc.add(schedule.getMonday());
        sc.add(schedule.getTuesday());
        sc.add(schedule.getWednesday());
        sc.add(schedule.getThursday());
        sc.add(schedule.getFriday());
        sc.add(schedule.getSaturday());
        sc.add(schedule.getSunday());
        boolean flag = true;
        int count = 0;
        int countDay = 0;

        for (ArrayList<SubAnime> anime: sc) {
            if (!flag) {
                flag = true;
                count += 1;
            }
            for (SubAnime subAnime: anime) {
                if (countDay < 6) {
                    flag = true;
                    countDay += 1;
                } else {
                    flag = false;
                    countDay = 0;
                }
                String title = String.format("%s\n%d - %s",
                        day[count], subAnime.getMalID(), subAnime.getTitle());
                event.reply(new EmbedBuilder()
                        .setTitle(title)
                        .setImage(subAnime.getImageURL())
                        .build());
            }
        }
    }
}
