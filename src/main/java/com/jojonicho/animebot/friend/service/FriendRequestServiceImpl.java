package com.jojonicho.animebot.friend.service;

import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.friend.model.FriendRequest;
import com.jojonicho.animebot.friend.repository.FriendRequestRepository;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FriendRequestServiceImpl implements FriendRequestService {
    @Autowired
    private FriendRequestRepository friendRequestRepository;

    @Override
    public FriendRequest createFriendRequest(DiscordUser user, DiscordUser friendUser,
                                             String userName, String friendName) {
        FriendRequest request = friendRequestRepository
                .findByDiscordUserAndFriendUser(user, friendUser);
        if (request == null) {
            request = friendRequestRepository.findByDiscordUserAndFriendUser(friendUser, user);
            if (request != null) {
                return request;
            }
            request = new FriendRequest(user, friendUser, userName, friendName);
            return friendRequestRepository.save(request);
        }

        return request;
    }

    @Override
    public Iterable<FriendRequest> getFriendRequestByFriend(DiscordUser friendUser) {
        return friendRequestRepository.findAllByFriendUser(friendUser);
    }

    @Override
    public Iterable<FriendRequest> getFriendRequestByUser(DiscordUser discordUser) {
        return friendRequestRepository.findAllByDiscordUser(discordUser);
    }

    @Override
    public FriendRequest getFriendRequest(DiscordUser discordUser, DiscordUser friendUser) {
        FriendRequest friendRequest = friendRequestRepository
                .findByDiscordUserAndFriendUser(discordUser, friendUser);
        if (friendRequest == null) {
            friendRequest = friendRequestRepository
                    .findByDiscordUserAndFriendUser(friendUser, discordUser);
        }
        return friendRequest;
    }

    @Override
    public void updateFriendRequestState(String state, FriendRequest friendRequest) {
        friendRequest.setState(state);
        friendRequestRepository.save(friendRequest);
    }

    @Override
    public void deleteFriendRequest(FriendRequest friendRequest) {
        friendRequestRepository.delete(friendRequest);
    }

    @Override
    public Map<String, DiscordUser> getAllFriend(DiscordUser discordUser) {
        Map<String, DiscordUser> friendList = new HashMap<String, DiscordUser>();

        Iterable<FriendRequest> acceptedRequestByFriend = friendRequestRepository
                .findAllByDiscordUserAndState(discordUser, "ACCEPTED");

        Iterable<FriendRequest> acceptedRequestByYou = friendRequestRepository
                .findAllByFriendUserAndState(discordUser, "ACCEPTED");

        if (acceptedRequestByFriend != null) {
            for (FriendRequest e: acceptedRequestByFriend) {
                friendList.put(e.getFriendName(), e.getFriendUser());
            }
        }

        if (acceptedRequestByYou != null) {
            for (FriendRequest e: acceptedRequestByYou) {
                friendList.put(e.getUserName(), e.getDiscordUser());
            }
        }

        return friendList;
    }
}
