package com.jojonicho.animebot.friend.state;

import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.friend.model.FriendRequest;
import com.jojonicho.animebot.friend.service.FriendRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AcceptedState implements State {
    @Autowired
    private FriendRequestService friendRequestService;

    @Override
    public String addFriend(DiscordUser user, DiscordUser friend, String friendName) {
        return String.format("%s already become your friend", friendName);
    }

    @Override
    public String acceptFriend(DiscordUser user, DiscordUser friend, String friendName) {
        return String.format("%s already become your friend", friendName);
    }

    @Override
    public String rejectFriend(DiscordUser user, DiscordUser friend, String friendName) {
        String response = " already become your friend.";
        String suggestion = " Reply $friend delete <name> to delete from your friend list.";
        return friendName + response + suggestion;
    }

    @Override
    public String deleteFriend(DiscordUser user, DiscordUser friend, String friendName) {
        FriendRequest friendRequest = friendRequestService.getFriendRequest(user, friend);
        friendRequestService.deleteFriendRequest(friendRequest);
        return String.format("Succesfully deleted %s from your friend list", friendName);
    }
}
