package com.jojonicho.animebot.friend.state;

import com.jojonicho.animebot.discorduser.model.DiscordUser;

public interface State {
    String addFriend(DiscordUser user, DiscordUser friend, String friendName);

    String acceptFriend(DiscordUser user, DiscordUser friend, String friendName);

    String rejectFriend(DiscordUser user, DiscordUser friend, String friendName);

    String deleteFriend(DiscordUser user, DiscordUser friend, String friendName);
}
