package com.jojonicho.animebot.friend.commands;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jojonicho.animebot.anime.model.AnimeEntry;
import com.jojonicho.animebot.anime.model.JikanAnime;
import com.jojonicho.animebot.anime.service.AnimeEntryService;
import com.jojonicho.animebot.anime.service.JikanAnimeService;
import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.discorduser.service.DiscordUserService;
import com.jojonicho.animebot.friend.service.FriendRequestService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.User;


public class FriendListCommand extends Command {
    private final FriendRequestService friendRequestService;
    private final DiscordUserService discordUserService;
    private final JikanAnimeService jikanAnimeService;
    private final AnimeEntryService animeEntryService;

    /**
     * Command to display friend that you added or odded you.
     */
    public FriendListCommand(FriendRequestService friendRequestService,
                             DiscordUserService discordUserService,
                             JikanAnimeService jikanAnimeService,
                             AnimeEntryService animeEntryService) {
        this.name = "friend-list";
        this.aliases = new String[]{"fl"};
        this.help = "get list of user that you added or added you";
        this.arguments = "<all/anime> <anime: name of anime>";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
        this.guildOnly = false;
        this.friendRequestService = friendRequestService;
        this.discordUserService = discordUserService;
        this.jikanAnimeService = jikanAnimeService;
        this.animeEntryService = animeEntryService;
    }

    @Override
    protected void execute(CommandEvent event) {
        String[] args = event.getArgs().split("\\s+");
        String choice = args[0];

        User user = event.getAuthor();
        String discordUserId = user.getId();
        DiscordUser discordUser = discordUserService.getDiscordUser(discordUserId);
        Map<String, DiscordUser> friendList;

        if (choice.equalsIgnoreCase("all")) {
            friendList = friendRequestService.getAllFriend(discordUser);
            displayUserList(event, friendList.keySet(), null, user.getEffectiveAvatarUrl());
        } else if (choice.equalsIgnoreCase("anime")) {
            List<String> result = new ArrayList<String>();
            String[] animeInfo = Arrays.copyOfRange(args, 1, args.length);
            JikanAnime anime = jikanAnimeService.getAnime(animeInfo);
            int mallId = anime.getMalId();
            friendList = friendRequestService.getAllFriend(discordUser);
            for (String friend: friendList.keySet()) {
                DiscordUser friendDiscord = friendList.get(friend);
                AnimeEntry animeEntry = animeEntryService.getAnimeEntry(mallId, friendDiscord);
                if (animeEntry != null) {
                    result.add(friend);
                }
            }
            String title = String.format("%d - %s", mallId, anime.getTitle());
            displayUserList(event, result, title, anime.getImageUrl());
        } else {
            event.reply("Invalid argument");
        }
    }

    private void displayUserList(CommandEvent event, Iterable<String> friendList,
                                 String judul, String image) {
        String title = "List Of Your Friend";
        String description = "";
        if (judul != null) {
            title = judul;
            description = "List of your friend that followed this anime:\n";
        }

        for (String friendName: friendList) {
            description += friendName + "\n";
        }

        event.reply(new EmbedBuilder()
                .setTitle(title)
                .setDescription(description)
                .setImage(image)
                .build()
        );
    }
}
