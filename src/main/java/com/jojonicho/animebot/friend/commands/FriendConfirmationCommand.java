package com.jojonicho.animebot.friend.commands;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.discorduser.service.DiscordUserService;
import com.jojonicho.animebot.friend.model.FriendRequest;
import com.jojonicho.animebot.friend.service.FriendRequestService;
import com.jojonicho.animebot.friend.state.State;
import com.jojonicho.animebot.friend.state.StateHelper;

import java.time.format.DateTimeFormatter;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.User;

public class FriendConfirmationCommand extends Command {
    private StateHelper stateHelper;
    private FriendRequestService friendRequestService;
    private DiscordUserService discordUserService;

    /**
     * Command to accept or reject friend.
     */
    public FriendConfirmationCommand(FriendRequestService friendRequestService,
                                     DiscordUserService discordUserService,
                                     StateHelper stateHelper) {
        this.name = "friend";
        this.aliases = new String[]{"f"};
        this.help = "accept/reject/delete friend";
        this.arguments = "<accept/reject/delete> <mention name>";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
        this.guildOnly = false;
        this.friendRequestService = friendRequestService;
        this.discordUserService = discordUserService;
        this.stateHelper = stateHelper;
    }

    @Override
    protected void execute(CommandEvent event) {
        String[] args = event.getArgs().split("\\s+");

        if (args.length == 2) {
            try {
                String command = args[0];
                String message;

                if (!command.equalsIgnoreCase("accept") && !command.equalsIgnoreCase("reject")
                        && !command.equalsIgnoreCase("delete")) {
                    message = "Invalid argument!";
                    replyMessageHandler(false, message, event, null);
                    return;
                }

                Member mention = event.getMessage().getMentionedMembers().get(0);
                User friend = mention.getUser();
                String friendId = friend.getId();
                DiscordUser friendUser = discordUserService.getDiscordUser(friendId);
                User user = event.getAuthor();
                String userId = user.getId();
                DiscordUser discordUser = discordUserService.getDiscordUser(userId);

                if (discordUser.equals(friendUser)) {
                    message = "Oh it's you, please select other user!";
                    replyMessageHandler(false, message, event, mention);
                    return;
                }

                FriendRequest req = friendRequestService.getFriendRequest(friendUser, discordUser);
                State requestState = stateHelper.getRequestState(req);
                String friendName = friend.getAsMention();

                if (command.equalsIgnoreCase("accept")) {
                    message = requestState.acceptFriend(discordUser, friendUser, friendName);
                    replyMessageHandler(true, message, event, mention);
                    return;
                }
                if (command.equalsIgnoreCase("reject")) {
                    message = requestState.rejectFriend(discordUser, friendUser, friendName);
                    replyMessageHandler(true, message, event, mention);
                    return;
                }
                message = requestState.deleteFriend(discordUser, friendUser, friendName);
                replyMessageHandler(true, message, event, mention);

            } catch (IndexOutOfBoundsException e) {
                String description = "You need to provide name as a mention!";
                replyMessageHandler(false, description, event, null);
            }
        } else {
            String description = "Invalid argument!";
            replyMessageHandler(false, description, event, null);
        }
    }

    private void replyMessageHandler(boolean existUser, String description,
                                     CommandEvent event, Member friend) {
        if (existUser) {
            String joinedTime = friend.getTimeJoined().format(DateTimeFormatter.ISO_DATE);
            event.reply(new EmbedBuilder()
                    .setDescription(description)
                    .addField("Name: ", friend.getUser().getName(), false)
                    .addField("Member since: ", joinedTime, false)
                    .setThumbnail(friend.getUser().getEffectiveAvatarUrl())
                    .build());
        } else {
            event.reply(new EmbedBuilder()
                    .setDescription(description)
                    .build());
        }
    }
}
