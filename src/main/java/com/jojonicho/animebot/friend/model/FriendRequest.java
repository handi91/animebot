package com.jojonicho.animebot.friend.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.jojonicho.animebot.discorduser.model.DiscordUser;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "friend_request")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FriendRequest {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", insertable = false, updatable = false, nullable = false)
    Long id;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "user_id", updatable = false, nullable = false)
    private DiscordUser discordUser;

    @Column(name = "user_name", nullable = false)
    private String userName;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "friend_id", updatable = false, nullable = false)
    private DiscordUser friendUser;

    @Column(name = "friend_name", nullable = false)
    private String friendName;

    @Column(name = "state", nullable = false)
    private String state;

    /**
     * Constructor.
     */
    public FriendRequest(DiscordUser discordUser, DiscordUser friendUser, String userName,
                         String friendName) {
        this.discordUser = discordUser;
        this.friendUser = friendUser;
        this.userName = userName;
        this.friendName = friendName;
        this.state = "IDLE";
    }

    public DiscordUser getDiscordUser() {
        return discordUser;
    }

    public DiscordUser getFriendUser() {
        return friendUser;
    }
}
