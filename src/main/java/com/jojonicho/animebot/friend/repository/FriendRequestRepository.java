package com.jojonicho.animebot.friend.repository;

import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.friend.model.FriendRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FriendRequestRepository extends JpaRepository<FriendRequest, String> {
    Iterable<FriendRequest> findAllByDiscordUser(DiscordUser discordUser);

    Iterable<FriendRequest> findAllByDiscordUserAndState(DiscordUser discorduser, String state);

    Iterable<FriendRequest> findAllByFriendUser(DiscordUser friendUser);
    
    Iterable<FriendRequest> findAllByFriendUserAndState(DiscordUser discordUser, String state);

    FriendRequest findByDiscordUserAndFriendUser(DiscordUser discordUser, DiscordUser friendUser);
}
