package com.jojonicho.animebot.manga.commands.crud;

import com.github.doomsdayrs.jikan4java.core.Connector;
import com.github.doomsdayrs.jikan4java.data.model.main.manga.Manga;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jojonicho.animebot.discorduser.service.DiscordUserService;
import com.jojonicho.animebot.manga.model.MangaEntry;
import com.jojonicho.animebot.manga.service.MangaEntryService;
import net.dv8tion.jda.api.EmbedBuilder;

public class ReadMangaEntry extends CrudMangaEntry {
    @Override
    public void execute(CommandEvent event,
                           MangaEntryService mangaEntryService,
                           DiscordUserService discordUserService) {
        String discordUserId = event.getAuthor().getId();
        Iterable<MangaEntry> mangaEntryList = mangaEntryService.getListMangaEntry(discordUserId);

        if (!mangaEntryList.iterator().hasNext()) {
            String title = "Manga list is empty!";
            String description = "It appears that you do not have anything on your manga list";

            event.reply(new EmbedBuilder()
                    .setTitle(title)
                    .setDescription(description)
                    .build());

            event.reactError();
            return;
        }

        String title = String.format("%s's manga list", event.getAuthor().getName());
        String mangaImageUrl = null;
        String avatarImageUrl = event.getAuthor().getAvatarUrl();
        StringBuilder description = new StringBuilder();

        for (MangaEntry mangaEntry : mangaEntryList) {
            int malId = mangaEntry.getMalId();
            Manga manga = new Connector().retrieveManga(malId).join();

            String mangaStatus = mangaEntry.getStatus() == null
                    ? "UNREAD"
                    : mangaEntry.getStatus();
            String mangaRating = mangaEntry.getRating() == 0
                    ? "UNRATED"
                    : Integer.toString(mangaEntry.getRating());

            description.append(String.format("%d - %s - %s - %s\n",
                    malId, manga.getTitle(), mangaStatus, mangaRating));

            if (null == mangaImageUrl) {
                mangaImageUrl = manga.getImageURL();
            }
        }

        event.reply(new EmbedBuilder()
                .setTitle(title)
                .setDescription(description.toString())
                .setImage(mangaImageUrl)
                .setThumbnail(avatarImageUrl)
                .build()
        );
    }
}