package com.jojonicho.animebot.manga.commands.crud;

import com.github.doomsdayrs.jikan4java.core.Connector;
import com.github.doomsdayrs.jikan4java.data.model.main.manga.Manga;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.discorduser.service.DiscordUserService;
import com.jojonicho.animebot.manga.model.MangaEntry;
import com.jojonicho.animebot.manga.service.MangaEntryService;
import java.util.concurrent.CompletionException;
import net.dv8tion.jda.api.EmbedBuilder;

public class DeleteMangaEntry extends CrudMangaEntry {
    @Override
    public void execute(CommandEvent event,
                           MangaEntryService mangaEntryService,
                           DiscordUserService discordUserService) {
        String[] args = event.getArgs().split("\\s+");
        int id = Integer.parseInt(args[1]);

        String discordUserId = event.getAuthor().getId();
        DiscordUser discordUser = discordUserService.getDiscordUser(discordUserId);

        MangaEntry mangaEntry = mangaEntryService.getMangaEntry(id, discordUser);

        if (null == mangaEntry) {
            try {
                Manga manga = new Connector().retrieveManga(id).join();
                int malId = manga.getMalID();
                mangaEntryNotFoundErrorReply(event, manga);
                return;
            } catch (CompletionException e) {
                String title = "Manga not found!";
                String description = String.format(
                        "It appears that we cannot find manga with the id: "
                        + "\"%d\" in our database.\nThus you cannot add/delete it from the list.",
                        id
                );

                event.reply(new EmbedBuilder()
                        .setTitle(title)
                        .setDescription(description)
                        .build());

                event.reactError();
            }
        }
        mangaEntryService.deleteMangaEntry(mangaEntry);

        String title = "Manga deleted!";
        String description = String.format(
                "Successfully deleted manga with the id: %d from your list",
                id
        );

        event.reply(new EmbedBuilder()
                .setTitle(title)
                .setDescription(description)
                .build());
    }
}
