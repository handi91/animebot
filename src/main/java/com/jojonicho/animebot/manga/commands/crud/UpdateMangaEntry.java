package com.jojonicho.animebot.manga.commands.crud;

import com.github.doomsdayrs.jikan4java.core.Connector;
import com.github.doomsdayrs.jikan4java.data.model.main.manga.Manga;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.discorduser.service.DiscordUserService;
import com.jojonicho.animebot.manga.model.MangaEntry;
import com.jojonicho.animebot.manga.service.MangaEntryService;

public class UpdateMangaEntry extends CrudMangaEntry {
    @Override
    public void execute(CommandEvent event,
                       MangaEntryService mangaEntryService,
                       DiscordUserService discordUserService) {
        String[] args = event.getArgs().split("\\s+");
        String command = args[0];
        int id = Integer.parseInt(args[1]);

        String newStatus = null;
        int rating = 0;

        if (args.length >= 4) {
            newStatus = args[2].toUpperCase();
            rating = Integer.parseInt(args[3]);
        }

        if (null == newStatus || rating == 0) {
            insufficientArgumentErrorReply(event);
            return;
        }

        String discordUserId = event.getAuthor().getId();
        DiscordUser discordUser = discordUserService.getDiscordUser(discordUserId);

        Manga manga = new Connector().retrieveManga(id).join();
        int malId = manga.getMalID();

        MangaEntry mangaEntry = mangaEntryService.getMangaEntry(malId, discordUser);
        if (null == mangaEntry) {
            mangaEntryNotFoundErrorReply(event, manga);
            return;
        }

        mangaEntry.setRating(rating);
        mangaEntry.setStatus(newStatus);
        mangaEntryService.updateMangaEntry(mangaEntry);
        event.reactSuccess();
    }
}
