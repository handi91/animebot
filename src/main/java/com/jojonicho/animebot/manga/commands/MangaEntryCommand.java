package com.jojonicho.animebot.manga.commands;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jojonicho.animebot.discorduser.service.DiscordUserService;
import com.jojonicho.animebot.manga.commands.crud.CreateMangaEntry;
import com.jojonicho.animebot.manga.commands.crud.CrudMangaEntry;
import com.jojonicho.animebot.manga.commands.crud.DeleteMangaEntry;
import com.jojonicho.animebot.manga.commands.crud.ReadMangaEntry;
import com.jojonicho.animebot.manga.commands.crud.UpdateMangaEntry;
import com.jojonicho.animebot.manga.service.MangaEntryService;
import net.dv8tion.jda.api.Permission;

public class MangaEntryCommand extends Command {
    private final MangaEntryService mangaEntryService;
    private final DiscordUserService discordUserService;

    /**
     * Method that adds CRUD functionality to Manga List.
     * @param mangaEntryService Manga entry service
     * @param discordUserService Discord user service
     */
    public MangaEntryCommand(MangaEntryService mangaEntryService,
                             DiscordUserService discordUserService) {
        this.name = "manga";
        this.aliases = new String[]{"m"};
        this.help = "add manga/manga to list";
        this.arguments = "<add/list/update/delete> <add manga_id> <update:status> <update:rating>";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
        this.guildOnly = false;
        this.mangaEntryService = mangaEntryService;
        this.discordUserService = discordUserService;
    }

    @Override
    public void execute(CommandEvent event) {
        String[] args = event.getArgs().split("\\s+");
        String command = args[0];

        CrudMangaEntry crudManga;

        if (command.equalsIgnoreCase("add")) {
            crudManga = new CreateMangaEntry();
        } else if (command.equalsIgnoreCase("list")) {
            crudManga = new ReadMangaEntry();
        } else if (command.equalsIgnoreCase("update")) {
            crudManga = new UpdateMangaEntry();
        } else if (command.equalsIgnoreCase("delete")) {
            crudManga = new DeleteMangaEntry();
        } else {
            crudManga = new CreateMangaEntry();
            crudManga.insufficientArgumentErrorReply(event);
            return;
        }

        crudManga.execute(event, mangaEntryService, discordUserService);
    }
}
