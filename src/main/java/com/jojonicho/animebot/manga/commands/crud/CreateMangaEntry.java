package com.jojonicho.animebot.manga.commands.crud;

import com.github.doomsdayrs.jikan4java.core.Connector;
import com.github.doomsdayrs.jikan4java.data.model.main.manga.Manga;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.discorduser.service.DiscordUserService;
import com.jojonicho.animebot.manga.model.MangaEntry;
import com.jojonicho.animebot.manga.service.MangaEntryService;
import java.util.concurrent.CompletionException;
import net.dv8tion.jda.api.EmbedBuilder;


public class CreateMangaEntry extends CrudMangaEntry {
    @Override
    public void execute(CommandEvent event,
                       MangaEntryService mangaEntryService,
                       DiscordUserService discordUserService) {
        String[] args = event.getArgs().split("\\s+");
        int id;

        try {
            id = Integer.parseInt(args[1]);
        } catch (NumberFormatException e) {
            incorrectArgumentErrorReply(event);
            return;
        } catch (ArrayIndexOutOfBoundsException e) {
            insufficientArgumentErrorReply(event);
            return;
        }

        String discordUserId = event.getAuthor().getId();
        DiscordUser discordUser = discordUserService.getDiscordUser(discordUserId);

        try {
            Manga manga = new Connector().retrieveManga(id).join();
            int malId = manga.getMalID();

            MangaEntry mangaEntry = mangaEntryService.getMangaEntry(malId, discordUser);

            if (null == mangaEntry) {
                mangaEntryService.createMangaEntry(malId, discordUser);

                String title = String.format(
                        "%d - %s", malId, manga.getTitle());
                String description = String.format(
                        "Successfully added %s to your list!", manga.getTitle());

                event.reply(new EmbedBuilder()
                        .setTitle(title)
                        .setDescription(description)
                        .setImage(manga.getImageURL())
                        .build());

                event.reactSuccess();
            } else {
                String title = "Manga already exists!";
                String description = String.format(
                        "You already have %s on your manga list", manga.getTitle());

                event.reply(new EmbedBuilder()
                        .setTitle(title)
                        .setDescription(description)
                        .build());

                event.reactError();
            }

        } catch (CompletionException e) {
            String title = "Manga not found!";
            String description = String.format(
                    "It appears that we cannot find manga with the id: \"%d\" in our database",
                    id
            );

            event.reply(new EmbedBuilder()
                    .setTitle(title)
                    .setDescription(description)
                    .build());

            event.reactError();
        }
    }
}
