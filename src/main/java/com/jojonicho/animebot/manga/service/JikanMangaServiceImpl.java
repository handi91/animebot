package com.jojonicho.animebot.manga.service;

import com.github.doomsdayrs.jikan4java.core.Retriever;
import com.jojonicho.animebot.manga.model.JikanManga;
import com.jojonicho.animebot.manga.model.JikanMangaList;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;

import org.springframework.stereotype.Service;

@Service
public class JikanMangaServiceImpl implements JikanMangaService {

    @Override
    public JikanManga getManga(String[] queryArgs) {
        String url = getUrl(queryArgs);
        return get(url);
    }

    @Override
    public JikanManga getManga(String query) {
        String url = getUrl(query);
        return get(url);
    }

    private JikanManga get(String url) {
        try {
            CompletableFuture<JikanMangaList> mangaSearch =
                    new Retriever().retrieve(url, JikanMangaList.class);

            JikanMangaList list = mangaSearch.join();
            return list.getResults().get(0);
        } catch (CompletionException e) {
            return null;
        }
    }

    private String getUrl(String[] args) {
        String query = String.join("%20", args);
        return String.format("https://api.jikan.moe/v3/search/manga?q=%s", query);
    }

    private String getUrl(String query) {
        return String.format("https://api.jikan.moe/v3/search/manga?q=%s", query);
    }
}
