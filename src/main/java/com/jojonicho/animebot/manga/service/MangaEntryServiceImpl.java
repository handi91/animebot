package com.jojonicho.animebot.manga.service;

import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.manga.model.MangaEntry;
import com.jojonicho.animebot.manga.repository.MangaEntryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MangaEntryServiceImpl implements MangaEntryService {
    @Autowired
    MangaEntryRepository mangaEntryRepository;

    @Override
    public MangaEntry createMangaEntry(int malId, DiscordUser discordUser) {

        MangaEntry mangaEntry = getMangaEntry(malId, discordUser);

        if (null != mangaEntry) {
            return mangaEntry;
        }

        mangaEntry = new MangaEntry();
        mangaEntry.setMalId(malId);
        mangaEntry.setDiscordUser(discordUser);

        return mangaEntryRepository.save(mangaEntry);
    }

    @Override
    public MangaEntry getMangaEntry(int malId, DiscordUser discordUser) {
        return mangaEntryRepository.findByMalIdAndDiscordUser(malId, discordUser);
    }

    @Override
    public Iterable<MangaEntry> getListMangaEntry(String userId) {
        return mangaEntryRepository.findAllByDiscordUser_Id(userId);
    }

    @Override
    public void deleteMangaEntry(MangaEntry mangaEntry) {
        mangaEntryRepository.delete(mangaEntry);
    }

    @Override
    public MangaEntry updateMangaEntry(MangaEntry mangaEntry) {
        return mangaEntryRepository.save(mangaEntry);
    }
}
