package com.jojonicho.animebot.manga.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import java.sql.Timestamp;
import lombok.Data;
import lombok.experimental.Accessors;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
@Data
@Accessors(chain = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class JikanManga {
    int malId;
    String url;
    String imageUrl;
    String title;
    boolean publishing;
    String synopsis;
    String type;
    int chapters;
    int volumes;
    double score;
    Timestamp startDate;
    Timestamp endDate;
    int members;
}
