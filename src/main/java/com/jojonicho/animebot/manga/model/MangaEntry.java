package com.jojonicho.animebot.manga.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.jojonicho.animebot.discorduser.model.DiscordUser;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * A model to represent Manga Log/Entry
 * for each Discord User.
 * Attributes:
 * - id (PK)
 * - malId -> Manga ID (identifier) on myanimelist
 * - discordUser -> User as an entry logger
 * - status -> (prob. watchlist/ongoing/watched/drop)
 * - rating -> int within the range of ...
 */
@Entity
@Table(name = "manga_entry")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MangaEntry {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id", insertable = false, updatable = false, nullable = false)
    Long id;

    @Column(name = "mal_id", updatable = false, nullable = false)
    private int malId;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "discord_user_id", updatable = false, nullable = false)
    private DiscordUser discordUser;

    @Column(name = "status")
    private String status;

    @Column(name = "rating")
    private int rating;
}
