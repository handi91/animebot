package com.jojonicho.animebot.discorduser.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "discord_user")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DiscordUser {

    @Id
    @Column(name = "id", updatable = false, nullable = false)
    private String id;
}
