package com.jojonicho.animebot.discorduser.service;

import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.discorduser.repository.DiscordUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DiscordUserServiceImpl implements DiscordUserService {

    @Autowired
    private DiscordUserRepository discordUserRepository;

    @Override
    public DiscordUser createDiscordUser(String id) {
        DiscordUser discordUser = new DiscordUser();
        discordUser.setId(id);
        return discordUserRepository.save(discordUser);
    }

    @Override
    public DiscordUser getDiscordUser(String id) {
        DiscordUser discordUser = discordUserRepository.getDiscordUserById(id);

        if (null == discordUser) {
            discordUser = new DiscordUser();
            discordUser.setId(id);
            discordUser = discordUserRepository.save(discordUser);
        }
        return discordUser;
    }

}
