package com.jojonicho.animebot.minigames.commands;

import com.github.doomsdayrs.jikan4java.core.Connector;
import com.github.doomsdayrs.jikan4java.data.exceptions.RequestError;
import com.github.doomsdayrs.jikan4java.data.model.main.anime.Anime;
import com.google.common.collect.Lists;
import com.google.errorprone.annotations.DoNotMock;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import com.jojonicho.animebot.anime.model.AnimeEntry;
import com.jojonicho.animebot.anime.service.AnimeEntryService;
import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.discorduser.service.DiscordUserService;
import java.util.Random;
import net.dv8tion.jda.api.EmbedBuilder;

public class SubstitutionCommand extends Command {
    private final EventWaiter waiter;
    private final DiscordUserService discordUserService;
    private final AnimeEntryService animeEntryService;

    /**
     * used for mainly accessing data when needed.
     *
     * @param waiter             used for timeout
     * @param animeEntryService  to access anime database
     * @param discordUserService to access discord user database
     */
    public SubstitutionCommand(EventWaiter waiter,
                               AnimeEntryService animeEntryService,
                               DiscordUserService discordUserService) {
        name = "minigame-substitution";
        aliases = new String[]{"mgs"};
        help = "starts substitution minigame";
        this.animeEntryService = animeEntryService;
        this.discordUserService = discordUserService;
        this.waiter = waiter;
    }

    @Override
    protected void execute(CommandEvent event) {
        String discordUserId = event.getAuthor().getId();

        Iterable<AnimeEntry> animeEntryList = animeEntryService.getListAnimeEntry(discordUserId);
        int maxSize = Lists.newArrayList(animeEntryList).size();
        Random random = new Random();

        int animenumber = random.nextInt(10000 - 1 + 1) + 1;

        int randnumber = 0;
        if (maxSize != 0) {
            randnumber = random.nextInt(maxSize);
        }

        int counter = 0;
        if (!animeEntryList.iterator().hasNext()) {
            event.reply("It seems you don't have anything on your anime list :(");
            return;
        } else {
            try {
                Anime anime = new Connector().retrieveAnime(animenumber).join();
                for (AnimeEntry e : animeEntryList) {
                    if (counter == randnumber && randnumber >= 0) {
                        animeEntryService.deleteAnimeEntry(e);
                        animeEntryService.createAnimeEntry(animenumber,
                                discordUserService.getDiscordUser(discordUserId));

                        String title = String.format("%d - %s", anime.getMalID(), anime.getTitle());
                        String description =
                                String.format("We deleted one of your Anime's "
                                        + "and replaced it with %s!", anime.getTitle());

                        event.reply(new EmbedBuilder()
                                .setTitle(title)
                                .setDescription(description)
                                .setImage(anime.getImageURL())
                                .build()
                        );
                        return;

                    } else {
                        counter += 1;
                    }
                }
            } catch (Exception e) {
                execute(event);
                return;
            }
        }
    }
}
