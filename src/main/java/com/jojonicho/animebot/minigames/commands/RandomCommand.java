package com.jojonicho.animebot.minigames.commands;

import com.github.doomsdayrs.jikan4java.core.Connector;
import com.github.doomsdayrs.jikan4java.data.model.main.anime.Anime;
import com.google.common.collect.Lists;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import com.jojonicho.animebot.anime.model.AnimeEntry;
import com.jojonicho.animebot.anime.service.AnimeEntryService;
import com.jojonicho.animebot.discorduser.service.DiscordUserService;
import com.sun.xml.bind.v2.model.core.ID;
import java.util.List;
import java.util.Random;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageReaction;


public class RandomCommand extends Command {
    private static final String EMOTE = "U+1F60D";
    private final EventWaiter waiter;
    private final AnimeEntryService animeEntryService;
    private final DiscordUserService discordUserService;
    private final Integer users = 0;
    private List<MessageReaction> reactA;


    /**
     * used for mainly accessing data when needed.
     *
     * @param waiter             used for timeout
     * @param animeEntryService  to access anime database
     * @param discordUserService to access discord user database
     */
    public RandomCommand(EventWaiter waiter,
                         AnimeEntryService animeEntryService,
                         DiscordUserService discordUserService) {
        name = "minigame-random";
        aliases = new String[]{"mgr"};
        help = "starts random minigame";
        this.animeEntryService = animeEntryService;
        this.discordUserService = discordUserService;
        this.waiter = waiter;

    }


    //    @Override
    @Override
    protected void execute(CommandEvent event) {


        String discordUserId = event.getAuthor().getId();

        Iterable<AnimeEntry> animeEntryList = animeEntryService.getListAnimeEntry(discordUserId);
        int maxSize = Lists.newArrayList(animeEntryList).size();
        Random random = new Random();
        int randnumber = 0;
        if (maxSize != 0) {
            randnumber = random.nextInt(maxSize);
        }
        int counter = 0;
        if (!animeEntryList.iterator().hasNext()) {
            event.reply("It seems you don't have anything on your anime list :(");
            return;
        } else {
            for (AnimeEntry e : animeEntryList) {
                if (counter == randnumber) {
                    int idAnime = e.getMalId();;
                    Anime anime = new Connector().retrieveAnime(idAnime).join();
                    String title = String.format("%d - %s", anime.getMalID(), anime.getTitle());
                    String description =
                            String.format("IT HAS BEEN CHOSEN! "
                                    + "we have randomly chosen to delete %s from your Anime List!",
                                    anime.getTitle());

                    event.reply(new EmbedBuilder()
                                    .setTitle(title)
                                    .setDescription(description)
                                    .setImage(anime.getImageURL())
                                    .build());
                    animeEntryService.deleteAnimeEntry(e);
                    return;
                } else {
                    counter += 1;
                }
            }
        }
    }
}
