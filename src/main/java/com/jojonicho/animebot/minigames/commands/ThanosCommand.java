package com.jojonicho.animebot.minigames.commands;

import com.google.common.collect.Lists;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import com.jojonicho.animebot.anime.model.AnimeEntry;
import com.jojonicho.animebot.anime.service.AnimeEntryService;
import com.jojonicho.animebot.discorduser.service.DiscordUserService;
import net.dv8tion.jda.api.EmbedBuilder;

public class ThanosCommand extends Command {
    //  private final EventWaiter waiter;
    //  private final DiscordUserService discordUserService;
    private final AnimeEntryService animeEntryService;

    /**
     * used for mainly accessing data when needed.
     *
     * @param waiter             used for timeout
     * @param animeEntryService  to access anime database
     * @param discordUserService to access discord user database
     */
    public ThanosCommand(EventWaiter waiter,
                         AnimeEntryService animeEntryService,
                         DiscordUserService discordUserService) {
        name = "minigame-thanos";
        aliases = new String[]{"mgt"};
        help = "starts thanos minigame";
        this.animeEntryService = animeEntryService;
        //        this.discordUserService = discordUserService;
        //        this.waiter = waiter;
    }

    @Override
    protected void execute(CommandEvent event) {

        String discordUserId = event.getAuthor().getId();

        Iterable<AnimeEntry> animeEntryList = animeEntryService.getListAnimeEntry(discordUserId);
        int maxSize = Lists.newArrayList(animeEntryList).size();

        int half = maxSize / 2;
        int counter = 0;
        if (!animeEntryList.iterator().hasNext()) {
            event.reply("It seems you don't have anything on your anime list :(");
            return;
        } else {
            for (AnimeEntry e : animeEntryList) {
                event.reply(new EmbedBuilder()
                        .setTitle("DELETED!")
                        .setDescription("with a single snap half of your anime list has vanished!")
                        .build()
                );;
                animeEntryService.deleteAnimeEntry(e);
                counter += 1;
                if (counter == half) {
                    break;


                }
            }

        }
    }
}
