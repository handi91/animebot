package com.jojonicho.animebot.minigames.commands;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jojonicho.animebot.anime.model.AnimeEntry;
import com.jojonicho.animebot.anime.service.AnimeEntryService;
import com.jojonicho.animebot.discorduser.model.DiscordUser;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class RandomCommandTest {

    @InjectMocks
    RandomCommand randomCommand;

    @Mock
    CommandEvent event;

    // demon slayer
    private final String ANIME_ID = "42249";
    private final String ANIME_ID_1 = "20";
    private final String ANIME_ID_2 = "19";

    private final String COMPLETED = "COMPLETED";
    private final String SCORE = "10";

    private final String DISCORD_ID = "264258056416657419";

    @Mock
    private AnimeEntryService animeEntryService;

    private DiscordUser discordUser;

    private AnimeEntry mockAnimeEntry;
    private AnimeEntry mockAnimeEntry1;
    private AnimeEntry mockAnimeEntry2;

    @Mock
    private User user;

    @BeforeEach
    public void setUp(){
        discordUser = new DiscordUser();
        discordUser.setId(DISCORD_ID);

        mockAnimeEntry = new AnimeEntry();
        mockAnimeEntry.setDiscordUser(discordUser);
        mockAnimeEntry.setMalId(Integer.parseInt(ANIME_ID));

        mockAnimeEntry1 = new AnimeEntry();
        mockAnimeEntry1.setDiscordUser(discordUser);
        mockAnimeEntry1.setMalId(Integer.parseInt(ANIME_ID_1));

        mockAnimeEntry2 = new AnimeEntry();
        mockAnimeEntry2.setDiscordUser(discordUser);
        mockAnimeEntry2.setMalId(Integer.parseInt(ANIME_ID_2));
    }

    @Test
    public void testExecute() {
        List<AnimeEntry> list = new ArrayList<>();
        list.add(mockAnimeEntry);
        list.add(mockAnimeEntry1);
        list.add(mockAnimeEntry2);

        when(event.getAuthor())
                .thenReturn(user);
        when(user.getId())
                .thenReturn(DISCORD_ID);
        when(animeEntryService.getListAnimeEntry(DISCORD_ID))
                .thenReturn(list);
        randomCommand.execute(event);
    }

    @Test
    public void testExecuteEmptyList() {
        List<AnimeEntry> list = new ArrayList<>();

        when(event.getAuthor())
                .thenReturn(user);
        when(user.getId())
                .thenReturn(DISCORD_ID);
        when(animeEntryService.getListAnimeEntry(DISCORD_ID))
                .thenReturn(list);
        randomCommand.execute(event);
    }

}

