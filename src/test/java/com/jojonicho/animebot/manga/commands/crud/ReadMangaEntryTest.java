package com.jojonicho.animebot.manga.commands.crud;

import static org.mockito.Mockito.when;

import com.jagrosh.jdautilities.command.CommandEvent;
import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.manga.commands.MangaEntryCommand;
import com.jojonicho.animebot.manga.model.MangaEntry;
import com.jojonicho.animebot.manga.service.MangaEntryService;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class ReadMangaEntryTest {

    @InjectMocks
    MangaEntryCommand mangaEntryCommand;

    @Mock
    CommandEvent event;

    private MangaEntry mockMangaEntry;

    private final String READ_ARGS = "list";

    private final String DISCORD_ID = "264258056416657419";

    @Mock
    private MangaEntryService mangaEntryService;

    @Mock
    private User user;

    @BeforeEach
    public void setUp() {
        final String MANGA_ID = "11";

        DiscordUser discordUser = new DiscordUser();
        discordUser.setId(DISCORD_ID);

        mockMangaEntry = new MangaEntry();
        mockMangaEntry.setDiscordUser(discordUser);
        mockMangaEntry.setMalId(Integer.parseInt(MANGA_ID));
    }

    @Test
    public void testExecuteRead() {
        List<MangaEntry> list = new ArrayList<>();
        list.add(mockMangaEntry);

        when(event.getAuthor())
                .thenReturn(user);
        when(user.getId())
                .thenReturn(DISCORD_ID);
        when(event.getArgs())
                .thenReturn(READ_ARGS);
        when(mangaEntryService.getListMangaEntry(DISCORD_ID))
                .thenReturn(list);
        mangaEntryCommand.execute(event);
    }

    @Test
    public void testExecuteReadEmptyList() {
        List<MangaEntry> list = new ArrayList<>();

        when(event.getAuthor())
                .thenReturn(user);
        when(user.getId())
                .thenReturn(DISCORD_ID);
        when(event.getArgs())
                .thenReturn(READ_ARGS);
        when(mangaEntryService.getListMangaEntry(DISCORD_ID))
                .thenReturn(list);
        mangaEntryCommand.execute(event);
    }
}