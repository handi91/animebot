package com.jojonicho.animebot.manga.commands;

import com.jagrosh.jdautilities.command.CommandEvent;
import com.jojonicho.animebot.manga.model.JikanManga;
import com.jojonicho.animebot.manga.model.MangaEntry;
import com.jojonicho.animebot.manga.service.JikanMangaServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class MangaSearchCommandTest {

    @InjectMocks
    MangaSearchCommand mangaSearchCommand;

    @Mock
    JikanMangaServiceImpl jikanMangaService;

    @Mock
    CommandEvent event;

    private String QUERY = "naruto";
    private String[] QUERY_ARGS = new String[]{"naruto"};

    @Test
    public void testExecute() {
        JikanManga manga = new JikanManga()
                .setImageUrl("https://cdn.myanimelist.net/images/manga/3/117681.jpg?s=6dc21454a32172a2e1783bd664668a22")
                .setMalId(11)
                .setTitle("Naruto")
                .setSynopsis("Whenever Naruto Uzumaki proclaims that he will someday " +
                        "become the Hokage—a title bestowed upon the best ninja in the " +
                        "Village Hidden in the Leaves—no one takes him seriously. Since " +
                        "birth, Naruto has be...");

        when(jikanMangaService.getManga(QUERY_ARGS))
                .thenReturn(manga);
        when(event.getArgs())
                .thenReturn(QUERY);
        mangaSearchCommand.execute(event);
    }

    @Test
    public void testExecuteSearchNonExistingManga() {
        when(event.getArgs())
                .thenReturn("0");
        mangaSearchCommand.execute(event);
    }
}
