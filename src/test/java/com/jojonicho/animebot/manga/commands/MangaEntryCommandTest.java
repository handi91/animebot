package com.jojonicho.animebot.manga.commands;

import static org.mockito.Mockito.when;

import com.jagrosh.jdautilities.command.CommandEvent;
import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.manga.model.MangaEntry;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class MangaEntryCommandTest {

    @InjectMocks
    MangaEntryCommand mangaEntryCommand;

    @Mock
    CommandEvent event;

    @BeforeEach
    public void setUp() {
        final String MANGA_ID = "11";
        final String DISCORD_ID = "264258056416657419";
        DiscordUser discordUser;
        MangaEntry mockMangaEntry;

        discordUser = new DiscordUser();
        discordUser.setId(DISCORD_ID);

        mockMangaEntry = new MangaEntry();
        mockMangaEntry.setDiscordUser(discordUser);
        mockMangaEntry.setMalId(Integer.parseInt(MANGA_ID));
    }

    @Test
    public void testExecuteEmptyArgs() {
        when(event.getArgs())
            .thenReturn("");
        mangaEntryCommand.execute(event);
    }

    @Test
    public void testExecuteWrongArgs() {
        when(event.getArgs())
                .thenReturn("updaet");
        mangaEntryCommand.execute(event);
    }

}