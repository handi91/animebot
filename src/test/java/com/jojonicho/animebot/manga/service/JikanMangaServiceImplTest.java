package com.jojonicho.animebot.manga.service;

import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.manga.model.JikanManga;
import com.jojonicho.animebot.manga.model.JikanMangaList;
import com.jojonicho.animebot.manga.model.MangaEntry;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.concurrent.CompletableFuture;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class JikanMangaServiceImplTest {

    @InjectMocks
    private JikanMangaServiceImpl jikanMangaService;

    @Mock
    private CompletableFuture<JikanMangaList> mangaSearch;

    private DiscordUser discordUser;

    private MangaEntry mockMangaEntry;

    private String QUERY = "naruto";

    private String URL = "https://api.jikan.moe/v3/search/manga?q=naruto";

    private String[] QUERY_ARGS = new String[]{"naruto"};


    @BeforeEach
    public void setUp() {
        discordUser = new DiscordUser();
        discordUser.setId("1");

        mockMangaEntry = new MangaEntry();
        mockMangaEntry.setDiscordUser(discordUser);
        mockMangaEntry.setMalId(1);
    }

    @Test
    public void testServiceCreate() {
        jikanMangaService.getManga(QUERY);
    }

    @Test
    public void testGetMangaAPI() {
        JikanManga manga = jikanMangaService.getManga(QUERY);
        assertEquals(manga.getTitle().substring(0, QUERY.length()).toLowerCase(),
                QUERY);
    }

    @Test
    public void testGetMangaAPIQueryArgs() {
        JikanManga manga = jikanMangaService.getManga(QUERY_ARGS);
        assertEquals(manga.getTitle().substring(0, QUERY.length()).toLowerCase(),
                QUERY);
    }
}
