package com.jojonicho.animebot.anime.commands;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

import com.github.doomsdayrs.jikan4java.data.model.main.season.SeasonSearchAnime;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import com.jojonicho.animebot.anime.model.AnimeEntry;
import com.jojonicho.animebot.anime.service.AnimeEntryService;
import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.discorduser.repository.DiscordUserRepository;
import com.jojonicho.animebot.discorduser.service.DiscordUserServiceImpl;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.MessageReaction;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.requests.RestAction;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class AnimeSeasonSearchCommandTest {

    @InjectMocks
    private AnimeSeasonSearchCommand animeSeasonSearchCommand;

    @Mock
    private CommandEvent event;

    private static final String INVALID_SEASON_ARGS = "2019 sumter";
    private static final String SUMMER_ARGS = "2019 summer";
    private static final String WINTER_ARGS = "2019 winter";
    private static final String SPRING_ARGS = "2019 spring";
    private static final String FALL_ARGS = "2019 fall";
    // demon slayer
    private static final String ANIME_ID = "40456";
    private static final String ADD_ARGS = "add " + ANIME_ID;
    private static final String DELETE_ARGS = "delete " + ANIME_ID;

    private static final String COMPLETED = "COMPLETED";
    private static final String SCORE = "10";
    private static final String UPDATE_ARGS = String.format("update %s %s %s",
        ANIME_ID, COMPLETED, SCORE);
    private static final String UPDATE_ARGS_INVALID = "update " + ANIME_ID;

    private static final String DISCORD_ID = "264258056416657419";

    @Mock
    private DiscordUserRepository discordUserRepository;

    @Mock
    private DiscordUserServiceImpl discordUserService;

    @Mock
    private AnimeEntryService animeEntryService;

    @Mock
    private EventWaiter waiter;

    @Mock
    private Message message;

    @Mock
    private RestAction<Void> restAction;

    @Mock
    private MessageReactionAddEvent messageReactionAddEvent;

    @Mock
    private MessageReaction messageReaction;

    @Mock
    private MessageReaction.ReactionEmote reactionEmote;

    @Mock
    private MessageChannel channel;

    @Mock
    private MessageAction messageAction;

    @Mock
    private MessageEmbed messageEmbed;

    @Mock
    private SeasonSearchAnime anime;

    private List<SeasonSearchAnime> animes;

    @Mock
    private User user;

    private DiscordUser discordUser;
    private AnimeEntry mockAnimeEntry;
    private final Long messageId = 696996L;
    private static final String LEFT_EMOJI = "👈";
    private static final String RIGHT_EMOJI = "👉";

    private final int malId = 38691;
    private final String imageUrl = "https://cdn.myanimelist.net/images/anime/1613/102576.jpg";
    private final String title = "Dr. Stone";
    private final String synopsis = "After five years of harboring unspoken feelings, "
        + "high-schooler "
        + "Taiju Ooki is finally ready to confess his love to Yuzuriha Ogawa."
        + "Just when Taiju begins "
        + "his confession however, a blinding green light strikes the Earth and petrifies mankind "
        + "around"
        + " the world—turning every single human into stone.\n"
        + "\n" + "Several millennia later, Taiju awakens to find the modern world "
        + "completely nonexistent" + ", as nature"
        + " has flourished in the years humanity stood still. Among a stone world of statues, "
        + "Taiju encounters "
        + "one other living human: his science-loving friend Senkuu, who has been active for a "
        + "few months. Taiju "
        + "learns that Senkuu has developed a grand scheme—to launch the complete revival of "
        + "civilization with science. "
        + "Taiju's brawn and Senkuu's brains combine to forge a formidable partnership, and "
        + "they soon uncover a method to"
        + " revive those petrified.\n"
        + "\n" + "However, Senkuu's master plan is threatened when his ideologies are challenged "
        + "by those who awaken. All the while,"
        + " the reason for mankind's petrification remains unknown.\n"
        + "\n" + "[Written by MAL Rewrite]";

    /**
     * setup.
     */
    @BeforeEach
    public void setUp() {
        discordUser = new DiscordUser();
        discordUser.setId(AnimeSeasonSearchCommandTest.DISCORD_ID);

        mockAnimeEntry = new AnimeEntry();
        mockAnimeEntry.setDiscordUser(discordUser);
        mockAnimeEntry.setMalId(malId);

        animeSeasonSearchCommand.setMessageId(1L);
        animeSeasonSearchCommand.isTest = true;
        animeSeasonSearchCommand.setAnimes(animes);
    }

    @Test
    public void testExecuteInvalidSeason() {
        List<AnimeEntry> list = new ArrayList<>();
        list.add(mockAnimeEntry);

        when(event.getArgs())
            .thenReturn(AnimeSeasonSearchCommandTest.INVALID_SEASON_ARGS);
        animeSeasonSearchCommand.execute(event);
    }

    @Test
    public void testExecuteSummerInitializePagination() {
        List<AnimeEntry> list = new ArrayList<>();
        list.add(mockAnimeEntry);

        when(event.getArgs())
            .thenReturn(AnimeSeasonSearchCommandTest.SUMMER_ARGS);
        when(message.getIdLong())
            .thenReturn(messageId);
        when(message.addReaction(anyString()))
            .thenReturn(restAction);

        doAnswer(ans -> {
            Consumer<Message> callback = (Consumer<Message>) ans.getArguments()[1];
            callback.accept(message);
            return null;
        }).when(event).reply(any(MessageEmbed.class), any(Consumer.class));

        animeSeasonSearchCommand.execute(event);
    }

    @Test
    public void testExecuteWinterRightEmoji() {
        when(event.getArgs())
            .thenReturn(AnimeSeasonSearchCommandTest.WINTER_ARGS);

        when(messageReactionAddEvent.getUserId())
            .thenReturn("1");
        when(event.getAuthor())
            .thenReturn(user);
        when(user.getId())
            .thenReturn("1");
        when(messageReactionAddEvent.getChannel())
            .thenReturn(channel);
        when(event.getChannel())
            .thenReturn(channel);

        when(messageReactionAddEvent.getReaction())
            .thenReturn(messageReaction);
        when(messageReaction.getReactionEmote())
            .thenReturn(reactionEmote);
        when(reactionEmote.getAsReactionCode())
            .thenReturn(RIGHT_EMOJI);

        when(channel.editMessageById(anyLong(), any(MessageEmbed.class)))
            .thenReturn(messageAction);


        doAnswer(ans -> {
            Predicate<MessageReactionAddEvent> predicate
                = (Predicate<MessageReactionAddEvent>) ans.getArguments()[1];
            predicate.test(messageReactionAddEvent);
            Consumer<MessageReactionAddEvent> callback
                = (Consumer<MessageReactionAddEvent>) ans.getArguments()[2];
            callback.accept(messageReactionAddEvent);
            return null;
        }).when(waiter).waitForEvent(any(),
            any(Predicate.class), any(Consumer.class));

        animeSeasonSearchCommand.execute(event);
    }

    @Test
    public void testExecuteWinterLeftEmoji() {
        when(event.getArgs())
            .thenReturn(AnimeSeasonSearchCommandTest.WINTER_ARGS);

        when(messageReactionAddEvent.getUserId())
            .thenReturn("1");
        when(event.getAuthor())
            .thenReturn(user);
        when(user.getId())
            .thenReturn("1");
        when(messageReactionAddEvent.getChannel())
            .thenReturn(channel);
        when(event.getChannel())
            .thenReturn(channel);

        when(messageReactionAddEvent.getReaction())
            .thenReturn(messageReaction);
        when(messageReaction.getReactionEmote())
            .thenReturn(reactionEmote);
        when(reactionEmote.getAsReactionCode())
            .thenReturn(LEFT_EMOJI);

        when(channel.editMessageById(anyLong(), any(MessageEmbed.class)))
            .thenReturn(messageAction);


        doAnswer(ans -> {
            Predicate<MessageReactionAddEvent> predicate
                = (Predicate<MessageReactionAddEvent>) ans.getArguments()[1];
            predicate.test(messageReactionAddEvent);
            Consumer<MessageReactionAddEvent> callback
                = (Consumer<MessageReactionAddEvent>) ans.getArguments()[2];
            callback.accept(messageReactionAddEvent);
            return null;
        }).when(waiter).waitForEvent(any(),
            any(Predicate.class), any(Consumer.class));

        animeSeasonSearchCommand.execute(event);
    }


    @Test
    public void testExecuteSpring() {
        List<AnimeEntry> list = new ArrayList<>();
        list.add(mockAnimeEntry);

        when(event.getArgs())
            .thenReturn(AnimeSeasonSearchCommandTest.SPRING_ARGS);
        animeSeasonSearchCommand.execute(event);
    }

    @Test
    public void testExecuteFall() {
        List<AnimeEntry> list = new ArrayList<>();
        list.add(mockAnimeEntry);

        when(event.getArgs())
            .thenReturn(AnimeSeasonSearchCommandTest.FALL_ARGS);
        animeSeasonSearchCommand.execute(event);
    }

    @Test
    public void testNegativePage() {
        animes = new ArrayList<>();
        animes.add(anime);
        animeSeasonSearchCommand.setPage(-1);
        animeSeasonSearchCommand.setAnimes(animes);

        when(anime.getMalID())
            .thenReturn(malId);

        animeSeasonSearchCommand.getMessageEmbedByPage();
    }

    @Test
    public void testOutOfBoundsPage() {
        animes = new ArrayList<>();
        animes.add(anime);
        animeSeasonSearchCommand.setPage(2);
        animeSeasonSearchCommand.setAnimes(animes);

        when(anime.getMalID())
            .thenReturn(malId);

        animeSeasonSearchCommand.getMessageEmbedByPage();
    }
}
