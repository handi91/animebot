package com.jojonicho.animebot.anime.commands;

import static org.mockito.Mockito.when;

import com.jagrosh.jdautilities.command.CommandEvent;
import com.jojonicho.animebot.anime.model.AnimeEntry;
import com.jojonicho.animebot.anime.service.AnimeEntryService;
import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.discorduser.repository.DiscordUserRepository;
import com.jojonicho.animebot.discorduser.service.DiscordUserServiceImpl;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class AnimeCommandTest {

    @InjectMocks
    private AnimeCommand animeCommand;

    @Mock
    private CommandEvent event;

    // demon slayer
    private static final String ANIME_ID = "40456";
    private static final String ADD_ARGS = "add " + ANIME_ID;
    private static final String DELETE_ARGS = "delete " + ANIME_ID;

    private static final String COMPLETED = "COMPLETED";
    private static final String SCORE = "10";
    private static final String UPDATE_ARGS = String.format("update %s %s %s",
        ANIME_ID, COMPLETED, SCORE);
    private static final String UPDATE_ARGS_INVALID = "update " + ANIME_ID;

    private static final String DISCORD_ID = "264258056416657419";

    @Mock
    private DiscordUserRepository discordUserRepository;

    @Mock
    private DiscordUserServiceImpl discordUserService;

    @Mock
    private AnimeEntryService animeEntryService;

    private DiscordUser discordUser;

    private AnimeEntry mockAnimeEntry;

    @Mock
    private User user;

    /**
     * setup.
     */
    @BeforeEach
    public void setUp() {
        discordUser = new DiscordUser();
        discordUser.setId(DISCORD_ID);

        mockAnimeEntry = new AnimeEntry();
        mockAnimeEntry.setDiscordUser(discordUser);
        mockAnimeEntry.setMalId(Integer.parseInt(ANIME_ID));
    }

    @Test
    public void testExecuteAdd() {
        when(event.getAuthor())
            .thenReturn(user);
        when(user.getId())
            .thenReturn(DISCORD_ID);
        when(event.getArgs())
            .thenReturn(ADD_ARGS);
        when(discordUserService.getDiscordUser(DISCORD_ID))
            .thenReturn(discordUser);
        when(animeEntryService.createAnimeEntry(mockAnimeEntry.getMalId(),
            discordUser))
            .thenReturn(mockAnimeEntry);
        animeCommand.execute(event);
    }

    @Test
    public void testExecuteDeleteNullAnimeEntry() {
        when(event.getAuthor())
            .thenReturn(user);
        when(user.getId())
            .thenReturn(DISCORD_ID);
        when(event.getArgs())
            .thenReturn(DELETE_ARGS);
        when(discordUserService.getDiscordUser(DISCORD_ID))
            .thenReturn(discordUser);
        animeCommand.execute(event);
    }

    @Test
    public void testExecuteDelete() {
        when(event.getAuthor())
            .thenReturn(user);
        when(user.getId())
            .thenReturn(DISCORD_ID);
        when(event.getArgs())
            .thenReturn(DELETE_ARGS);
        when(discordUserService.getDiscordUser(DISCORD_ID))
            .thenReturn(discordUser);
        when(animeEntryService.getAnimeEntry(mockAnimeEntry.getMalId(), discordUser))
            .thenReturn(mockAnimeEntry);
        animeCommand.execute(event);
    }

    @Test
    public void testExecuteEmptyArgs() {
        when(event.getArgs())
            .thenReturn("");
        animeCommand.execute(event);
    }

    @Test
    public void testExecuteUpdateNullAnimeEntry() {
        when(event.getAuthor())
            .thenReturn(user);
        when(user.getId())
            .thenReturn(DISCORD_ID);
        when(event.getArgs())
            .thenReturn(UPDATE_ARGS);
        when(discordUserService.getDiscordUser(DISCORD_ID))
            .thenReturn(discordUser);
        animeCommand.execute(event);
    }

    @Test
    public void testExecuteUpdateFail() {
        when(event.getArgs())
            .thenReturn(UPDATE_ARGS_INVALID);
        animeCommand.execute(event);
    }

    @Test
    public void testExecuteUpdateSuccess() {
        when(event.getAuthor())
            .thenReturn(user);
        when(user.getId())
            .thenReturn(DISCORD_ID);
        when(event.getArgs())
            .thenReturn(UPDATE_ARGS);
        when(discordUserService.getDiscordUser(DISCORD_ID))
            .thenReturn(discordUser);
        when(animeEntryService.getAnimeEntry(mockAnimeEntry.getMalId(), discordUser))
            .thenReturn(mockAnimeEntry);
        animeCommand.execute(event);
    }
}
