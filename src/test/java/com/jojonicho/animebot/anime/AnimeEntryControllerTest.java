package com.jojonicho.animebot.anime;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jojonicho.animebot.anime.model.AnimeEntry;
import com.jojonicho.animebot.anime.model.AnimeEntryRequest;
import com.jojonicho.animebot.anime.service.AnimeEntryServiceImpl;
import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.discorduser.service.DiscordUserServiceImpl;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;


@SpringBootTest
@AutoConfigureMockMvc
public class AnimeEntryControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private AnimeEntryServiceImpl animeEntryService;
    @MockBean
    private DiscordUserServiceImpl discordUserService;

    private AnimeEntry mockAnimeEntry;
    private AnimeEntryRequest req;

    private DiscordUser discordUser;
    private static final String discordId = "264258056416657419"; // jojonicho

    private final int malId = 40456;
    private final int rating = 10;
    private final String status = "COMPLETED";

    private static final String BASE_URL = "/anime-entry/";
    private static final String DISCORD_ID_URL = BASE_URL + discordId;
    private static final String DISCORD_ID_404_URL = BASE_URL + 69420;

    @BeforeEach
    void setUp() {
        req = new AnimeEntryRequest();
        req.setMalId(malId); // demon slayer mugen train
        req.setRating(rating);
        req.setStatus(status);

        discordUser = new DiscordUser();
        discordUser.setId(discordId);

        mockAnimeEntry = new AnimeEntry();
        mockAnimeEntry.setMalId(req.getMalId());
        mockAnimeEntry.setRating(req.getRating());
        mockAnimeEntry.setStatus(req.getStatus());
        mockAnimeEntry.setDiscordUser(discordUser);

    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    public void testControllerGetListNonExist() throws Exception {
        when(animeEntryService.getListAnimeEntry("69420"))
            .thenReturn(null);
        mvc.perform(get(DISCORD_ID_404_URL))
            .andExpect(status().isNotFound());
    }

    @Test
    public void testControllerGetList() throws Exception {
        List<AnimeEntry> list = new ArrayList<>();
        when(discordUserService.getDiscordUser(discordId))
            .thenReturn(discordUser);
        when(animeEntryService.getListAnimeEntry(discordId))
            .thenReturn(list);
        mvc.perform(get(DISCORD_ID_URL))
            .andExpect(status().isOk());
    }

    @Test
    public void testControllerPostIncorrectRequest() throws Exception {
        when(discordUserService.getDiscordUser(discordId))
            .thenReturn(discordUser);
        when(animeEntryService.createAnimeEntry(malId, discordUser))
            .thenReturn(mockAnimeEntry);
        mvc.perform(post(BASE_URL + discordUser)
            .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(mockAnimeEntry)))
            .andExpect(status().is4xxClientError());
    }

    @Test
    public void testControllerPost() throws Exception {
        when(discordUserService.getDiscordUser(discordId))
            .thenReturn(discordUser);
        when(animeEntryService.createAnimeEntry(malId, discordUser))
            .thenReturn(mockAnimeEntry);
        mvc.perform(post(DISCORD_ID_URL)
            .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(req)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.malId").value(malId));
    }

    @Test
    public void testControllerUpdateNonExist() throws Exception {
        AnimeEntry updatedAnimeEntry = new AnimeEntry();
        updatedAnimeEntry.setStatus("DROPPED");
        updatedAnimeEntry.setRating(mockAnimeEntry.getRating());
        updatedAnimeEntry.setMalId(mockAnimeEntry.getMalId());
        updatedAnimeEntry.setMalId(mockAnimeEntry.getMalId());

        when(discordUserService.getDiscordUser(discordId))
            .thenReturn(discordUser);
        when(animeEntryService.getAnimeEntry(malId, discordUser))
            .thenReturn(mockAnimeEntry);
        when(animeEntryService.updateAnimeEntry(mockAnimeEntry))
            .thenReturn(updatedAnimeEntry);

        req.setMalId(69420);
        mvc.perform(put(DISCORD_ID_URL)
            .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(req)))
            .andExpect(status().isNotFound());
    }

    @Test
    public void testControllerUpdate() throws Exception {
        AnimeEntry updatedAnimeEntry = new AnimeEntry();
        updatedAnimeEntry.setStatus("DROPPED");
        updatedAnimeEntry.setRating(mockAnimeEntry.getRating());
        updatedAnimeEntry.setMalId(mockAnimeEntry.getMalId());
        updatedAnimeEntry.setMalId(mockAnimeEntry.getMalId());

        when(discordUserService.getDiscordUser(discordId))
            .thenReturn(discordUser);
        when(animeEntryService.getAnimeEntry(malId, discordUser))
            .thenReturn(mockAnimeEntry);
        when(animeEntryService.updateAnimeEntry(mockAnimeEntry))
            .thenReturn(updatedAnimeEntry);
        mvc.perform(put(DISCORD_ID_URL)
            .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(req)))
            .andExpect(jsonPath("$.malId").value(malId))
            .andExpect(jsonPath("$.status").value("DROPPED"));
    }


    @Test
    public void testControllerDelete() throws Exception {
        when(animeEntryService.createAnimeEntry(malId, discordUser))
            .thenReturn(mockAnimeEntry);
        when(discordUserService.getDiscordUser(discordId))
            .thenReturn(discordUser);
        mvc.perform(delete(DISCORD_ID_URL + "/" + malId))
            .andExpect(status().isOk());
    }


}
