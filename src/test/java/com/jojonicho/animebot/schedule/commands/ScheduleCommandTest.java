package com.jojonicho.animebot.schedule.commands;

import com.jagrosh.jdautilities.command.CommandEvent;
import com.jojonicho.animebot.schedule.commands.ScheduleCommand;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ScheduleCommandTest {

    @InjectMocks
    ScheduleCommand scheduleCommand;

    @Mock
    CommandEvent event;

    @Mock
    private User user;

    @Test
    public void testExecute() {

        scheduleCommand.execute(event);

    }
}

