package com.jojonicho.animebot.friend.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.friend.model.FriendRequest;
import com.jojonicho.animebot.friend.repository.FriendRequestRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class FriendRequestServiceImplTest {
    @InjectMocks
    private FriendRequestServiceImpl friendRequestService;

    @Mock
    private FriendRequestRepository friendRequestRepository;

    private DiscordUser discordUser;
    private DiscordUser friendUser;
    private FriendRequest mockFriendRequest;

    @BeforeEach
    public void setUp() {
        discordUser = new DiscordUser();
        discordUser.setId("1");

        friendUser = new DiscordUser();
        friendUser.setId("2");
        mockFriendRequest = new FriendRequest();
        mockFriendRequest.setDiscordUser(discordUser);
        mockFriendRequest.setFriendUser(friendUser);
        mockFriendRequest.setUserName("dummy");
        mockFriendRequest.setFriendName("dummy2");
        mockFriendRequest.setState("IDLE");
    }

    @Test
    public void testServiceCreate() {
        when(friendRequestRepository.save(any()))
                .thenReturn(mockFriendRequest);
        FriendRequest friendRequest = friendRequestService.createFriendRequest(discordUser, friendUser, "dummy", "dummy2");
        assertEquals(mockFriendRequest, friendRequest);
    }

    @Test
    public void testServiceCreateDuplicate() {
        when(friendRequestRepository.findByDiscordUserAndFriendUser(mockFriendRequest.getDiscordUser(), mockFriendRequest.getFriendUser()))
                .thenReturn(mockFriendRequest);

        FriendRequest friendRequest = friendRequestService.createFriendRequest(discordUser, friendUser, "dummy", "dummy2");
        assertEquals(friendRequest, mockFriendRequest);

        when(friendRequestRepository.findByDiscordUserAndFriendUser(mockFriendRequest.getDiscordUser(), mockFriendRequest.getFriendUser()))
                .thenReturn(null);
        when(friendRequestRepository.findByDiscordUserAndFriendUser(mockFriendRequest.getFriendUser(), mockFriendRequest.getDiscordUser()))
                .thenReturn(mockFriendRequest);

        friendRequest = friendRequestService.createFriendRequest(discordUser, friendUser, "dummy", "dummy2");
        assertEquals(friendRequest, mockFriendRequest);
    }

    @Test
    public void testServiceFindByFriendUser() {
        List<FriendRequest> list = new ArrayList<>();
        list.add(mockFriendRequest);

        when(friendRequestRepository.findAllByFriendUser(friendUser))
                .thenReturn(list);

        Iterable<FriendRequest> requestList = friendRequestService.getFriendRequestByFriend(friendUser);
        assertEquals(list, requestList);
    }

    @Test
    public void testServiceFindByUser() {
        List<FriendRequest> list = new ArrayList<>();
        list.add(mockFriendRequest);

        when(friendRequestRepository.findAllByDiscordUser(discordUser))
                .thenReturn(list);

        Iterable<FriendRequest> requestList = friendRequestService.getFriendRequestByUser(discordUser);
        assertEquals(list, requestList);
    }

    @Test
    public void testServiceFindByUserAndFriend() {
        when(friendRequestRepository.findByDiscordUserAndFriendUser(discordUser, friendUser))
                .thenReturn(mockFriendRequest);
        FriendRequest request = friendRequestService.getFriendRequest(discordUser, friendUser);
        assertEquals(request, mockFriendRequest);

        when(friendRequestRepository.findByDiscordUserAndFriendUser(discordUser, friendUser))
                .thenReturn(null);
        when(friendRequestRepository.findByDiscordUserAndFriendUser(friendUser, discordUser))
                .thenReturn(mockFriendRequest);
        request = friendRequestService.getFriendRequest(discordUser, friendUser);
        assertEquals(request, mockFriendRequest);
    }

    @Test
    public void testServiceUpdateRequestState() {
        friendRequestService.createFriendRequest(discordUser, friendUser, "dummy", "dummy2");
        String state = "ADDED";
        FriendRequest expected = mockFriendRequest;
        expected.setState(state);
        friendRequestService.updateFriendRequestState(state, mockFriendRequest);
        assertEquals(expected, mockFriendRequest);
    }

    @Test
    public void testServiceDelete() {
        friendRequestService.createFriendRequest(discordUser, friendUser, "dummy", "dummy2");
        FriendRequest request = friendRequestService.getFriendRequest(discordUser, friendUser);
        friendRequestService.deleteFriendRequest(request);
        assertEquals(null, friendRequestService.getFriendRequest(discordUser, friendUser));
    }

    @Test
    public void testServiceGetFriend() {
        Map<String, DiscordUser> friends = new HashMap<String, DiscordUser>();
        friends.put(mockFriendRequest.getFriendName(), friendUser);
        friends.put(mockFriendRequest.getUserName(), discordUser);

        List<FriendRequest> list = new ArrayList<>();
        list.add(mockFriendRequest);
        when(friendRequestRepository.findAllByDiscordUserAndState(discordUser, "ACCEPTED"))
                .thenReturn(list);
        when(friendRequestRepository.findAllByFriendUserAndState(discordUser, "ACCEPTED"))
                .thenReturn(list);
        Map<String, DiscordUser> friendList = friendRequestService.getAllFriend(discordUser);
        assertEquals(friendList, friends);

        when(friendRequestRepository.findAllByDiscordUserAndState(friendUser, "ACCEPTED"))
                .thenReturn(null);
        when(friendRequestRepository.findAllByFriendUserAndState(friendUser, "ACCEPTED"))
                .thenReturn(null);
        Map<String, DiscordUser> friendList2 = friendRequestService.getAllFriend(friendUser);
        assertEquals(friendList2, new HashMap<>());
    }
}
