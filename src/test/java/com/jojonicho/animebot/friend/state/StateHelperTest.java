package com.jojonicho.animebot.friend.state;

import com.jojonicho.animebot.friend.model.FriendRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class StateHelperTest {
    @InjectMocks
    StateHelper stateHelper;

    @Mock
    IdleState idleState;

    @Mock
    AddedState addedState;

    @Mock
    AcceptedState acceptedState;

    @Mock
    FriendRequest friendRequest;

    @Test
    public void testGetRequestState() {
        when(friendRequest.getState())
                .thenReturn("IDLE");
        assertEquals(idleState, stateHelper.getRequestState(friendRequest));
        when(friendRequest.getState())
                .thenReturn("ADDED");
        assertEquals(addedState, stateHelper.getRequestState(friendRequest));
        when(friendRequest.getState())
                .thenReturn("ACCEPTED");
        assertEquals(acceptedState, stateHelper.getRequestState(friendRequest));

        when(friendRequest.getState())
                .thenThrow(new NullPointerException());
        assertEquals(idleState, stateHelper.getRequestState(friendRequest));
    }
}
