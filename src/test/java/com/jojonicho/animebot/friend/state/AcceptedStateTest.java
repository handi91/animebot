package com.jojonicho.animebot.friend.state;

import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.friend.model.FriendRequest;
import com.jojonicho.animebot.friend.service.FriendRequestService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AcceptedStateTest {
    @InjectMocks
    AcceptedState acceptedState;

    @Mock
    FriendRequestService friendRequestService;

    private FriendRequest friendRequest;
    private DiscordUser discordUser;
    private DiscordUser friendUser;

    @BeforeEach
    public void setUp() {
        discordUser = new DiscordUser();
        discordUser.setId("1223");
        friendUser = new DiscordUser();
        friendUser.setId("1224");

        friendRequest = new FriendRequest();
        friendRequest.setDiscordUser(discordUser);
        friendRequest.setFriendUser(friendUser);
        friendRequest.setUserName("dummy");
        friendRequest.setFriendName("dummy2");
        friendRequest.setState("ACCEPTED");
    }

    @Test
    public void testAdd() {
        String result = acceptedState.addFriend(discordUser, friendUser, "dummy");
        assertEquals(result, "dummy already become your friend");
    }

    @Test
    public void testAccept() {
        String result = acceptedState.acceptFriend(discordUser, friendUser, "dummy");
        assertEquals(result, "dummy already become your friend");
    }

    @Test
    public void testReject() {
        String result = acceptedState.rejectFriend(discordUser, friendUser, "dummy");
        assertEquals(result, "dummy already become your friend. Reply $friend delete <name> to delete from your friend list.");
    }

    @Test
    public void testDelete() {
        when(friendRequestService.getFriendRequest(discordUser, friendUser))
                .thenReturn(friendRequest);
        String result = acceptedState.deleteFriend(discordUser, friendUser, "dummy");
        assertEquals(result, "Succesfully deleted dummy from your friend list");
    }
}
