package com.jojonicho.animebot.friend.commands;

import com.jagrosh.jdautilities.command.CommandEvent;
import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.discorduser.service.DiscordUserServiceImpl;
import com.jojonicho.animebot.friend.model.FriendRequest;
import com.jojonicho.animebot.friend.service.FriendRequestService;
import com.jojonicho.animebot.friend.state.AcceptedState;
import com.jojonicho.animebot.friend.state.AddedState;
import com.jojonicho.animebot.friend.state.IdleState;
import com.jojonicho.animebot.friend.state.StateHelper;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class FriendConfirmationCommandTest {
    @InjectMocks
    FriendConfirmationCommand friendConfirmationCommand;

    @Mock
    CommandEvent event;

    @Mock
    private DiscordUserServiceImpl discordUserService;

    @Mock
    private FriendRequestService friendRequestService;

    @Mock
    private StateHelper stateHelper;

    @Mock
    private IdleState idleState;

    @Mock
    private AddedState addedState;

    @Mock
    private AcceptedState acceptedState;

    @Mock
    private Message message;

    @Mock
    private User user;

    @Mock
    private User friend;

    @Mock
    private Member member;

    private DiscordUser discordUser;
    private DiscordUser friendUser;
    private FriendRequest mockFriendRequest;
    private final String DISCORD_ID = "264258056416657419";
    private final String FRIEND_ID = "708209585407655966";
    private final String ACCEPT_ARGS = "accept @dummy";
    private final String REJECT_ARGS = "reject @dummy";
    private final String DELETE_ARGS = "delete @dummy";

    @BeforeEach
    public void setUp(){
        discordUser = new DiscordUser();
        discordUser.setId(DISCORD_ID);

        friendUser = new DiscordUser();
        friendUser.setId(FRIEND_ID);

        mockFriendRequest = new FriendRequest();
        mockFriendRequest.setDiscordUser(discordUser);
        mockFriendRequest.setFriendUser(friendUser);
        mockFriendRequest.setUserName("dummy");
        mockFriendRequest.setFriendName("dummy2");
        mockFriendRequest.setState("ADDED");
    }

    @Test
    public void testSuccessfullyAccepted() {
        List<Member> list = new ArrayList<Member>();
        list.add(member);

        when(event.getArgs())
                .thenReturn(ACCEPT_ARGS);
        when(event.getAuthor())
                .thenReturn(user);
        when(user.getId())
                .thenReturn(DISCORD_ID);
        when(discordUserService.getDiscordUser(DISCORD_ID))
                .thenReturn(discordUser);
        when(event.getMessage())
                .thenReturn(message);
        when(message.getMentionedMembers())
                .thenReturn(list);
        when(member.getUser())
                .thenReturn(friend);
        when(friend.getId())
                .thenReturn(FRIEND_ID);
        when(discordUserService.getDiscordUser(FRIEND_ID))
                .thenReturn(friendUser);
        when(friendRequestService.getFriendRequest(friendUser, discordUser))
                .thenReturn(mockFriendRequest);
        when(stateHelper.getRequestState(mockFriendRequest))
                .thenReturn(addedState);
        when(friend.getName())
                .thenReturn("Dummy");
        when(member.getTimeJoined())
                .thenReturn(OffsetDateTime.MIN);
        friendConfirmationCommand.execute(event);
    }
    @Test
    public void testSuccessfullyRejected() {
        List<Member> list = new ArrayList<Member>();
        list.add(member);

        when(event.getArgs())
                .thenReturn(REJECT_ARGS);
        when(event.getAuthor())
                .thenReturn(user);
        when(user.getId())
                .thenReturn(DISCORD_ID);
        when(discordUserService.getDiscordUser(DISCORD_ID))
                .thenReturn(discordUser);
        when(event.getMessage())
                .thenReturn(message);
        when(message.getMentionedMembers())
                .thenReturn(list);
        when(member.getUser())
                .thenReturn(friend);
        when(friend.getId())
                .thenReturn(FRIEND_ID);
        when(discordUserService.getDiscordUser(FRIEND_ID))
                .thenReturn(friendUser);
        when(friendRequestService.getFriendRequest(friendUser, discordUser))
                .thenReturn(mockFriendRequest);
        when(stateHelper.getRequestState(mockFriendRequest))
                .thenReturn(addedState);
        when(friend.getName())
                .thenReturn("Dummy");
        when(member.getTimeJoined())
                .thenReturn(OffsetDateTime.MIN);
        friendConfirmationCommand.execute(event);
    }

    @Test
    public void testSuccesfulDeleted() {
        List<Member> list = new ArrayList<Member>();
        list.add(member);
        mockFriendRequest.setState("ACCEPTED");

        when(event.getArgs())
                .thenReturn(DELETE_ARGS);
        when(event.getAuthor())
                .thenReturn(user);
        when(user.getId())
                .thenReturn(DISCORD_ID);
        when(discordUserService.getDiscordUser(DISCORD_ID))
                .thenReturn(discordUser);
        when(event.getMessage())
                .thenReturn(message);
        when(message.getMentionedMembers())
                .thenReturn(list);
        when(member.getUser())
                .thenReturn(friend);
        when(friend.getId())
                .thenReturn(FRIEND_ID);
        when(discordUserService.getDiscordUser(FRIEND_ID))
                .thenReturn(friendUser);
        when(friendRequestService.getFriendRequest(friendUser, discordUser))
                .thenReturn(mockFriendRequest);
        when(stateHelper.getRequestState(mockFriendRequest))
                .thenReturn(acceptedState);
        when(friend.getName())
                .thenReturn("Dummy");
        when(member.getTimeJoined())
                .thenReturn(OffsetDateTime.MIN);
        friendConfirmationCommand.execute(event);
    }

    @Test
    public void testAcceptOrRejectOrDeleteSelf() {
        List<Member> list = new ArrayList<Member>();
        list.add(member);

        when(event.getArgs())
                .thenReturn(REJECT_ARGS);
        when(event.getAuthor())
                .thenReturn(user);
        when(user.getId())
                .thenReturn(DISCORD_ID);
        when(discordUserService.getDiscordUser(DISCORD_ID))
                .thenReturn(discordUser);
        when(event.getMessage())
                .thenReturn(message);
        when(message.getMentionedMembers())
                .thenReturn(list);
        when(member.getUser())
                .thenReturn(friend);
        when(friend.getId())
                .thenReturn(FRIEND_ID);
        when(discordUserService.getDiscordUser(FRIEND_ID))
                .thenReturn(discordUser);
        friendConfirmationCommand.execute(event);
    }

    @Test
    public void testInsufficientOrTooMuchArgument() {
        when(event.getArgs())
                .thenReturn("");
        friendConfirmationCommand.execute(event);
        when(event.getArgs())
                .thenReturn("accept friend @dummy");
        friendConfirmationCommand.execute(event);
    }

    @Test
    public void testInvalidArgument() {
        when(event.getArgs())
                .thenReturn("kick @dummy");
        friendConfirmationCommand.execute(event);
    }

    @Test
    public void testInvalidFriendName() {
        when(event.getArgs())
                .thenReturn(REJECT_ARGS);
        when(event.getMessage())
                .thenReturn(message);
        when(message.getMentionedMembers())
                .thenThrow(new IndexOutOfBoundsException());
        friendConfirmationCommand.execute(event);
    }
}
