package com.jojonicho.animebot.friend.commands;

import static org.mockito.Mockito.when;

import com.jagrosh.jdautilities.command.CommandEvent;
import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.discorduser.service.DiscordUserServiceImpl;
import com.jojonicho.animebot.friend.model.FriendRequest;
import com.jojonicho.animebot.friend.service.FriendRequestService;

import java.util.ArrayList;
import java.util.List;

import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class AddFriendListCommandTest {
    @InjectMocks
    AddFriendListCommand addFriendListCommand;

    @Mock
    CommandEvent event;

    @Mock
    private DiscordUserServiceImpl discordUserService;

    @Mock
    private FriendRequestService friendRequestService;

    @Mock
    private User user;

    @Mock
    private User friend;

    private DiscordUser discordUser;
    private DiscordUser friendUser;
    private FriendRequest mockFriendRequest;
    private final String DISCORD_ID = "264258056416657419";
    private final String FRIEND_ID = "708209585407655966";

    private final String ME_ARGS = "me";
    private final String OTHER_ARGS = "other";

    @BeforeEach
    public void setUp() {
        discordUser = new DiscordUser();
        discordUser.setId(DISCORD_ID);

        friendUser = new DiscordUser();
        friendUser.setId(FRIEND_ID);

        mockFriendRequest = new FriendRequest();
        mockFriendRequest.setDiscordUser(discordUser);
        mockFriendRequest.setFriendUser(friendUser);
        mockFriendRequest.setUserName("dummy");
        mockFriendRequest.setFriendName("dummy2");
        mockFriendRequest.setState("ACCEPTED");
    }

    @Test
    public void testAddFriendListByUser() {
        List<FriendRequest> list = new ArrayList<>();
        list.add(mockFriendRequest);
        when(event.getAuthor())
                .thenReturn(user);
        when(user.getId())
                .thenReturn(DISCORD_ID);
        when(event.getArgs())
                .thenReturn(ME_ARGS);
        when(discordUserService.getDiscordUser(DISCORD_ID))
                .thenReturn(discordUser);
        when(friendRequestService.getFriendRequestByUser(discordUser))
                .thenReturn(list);
        addFriendListCommand.execute(event);

        mockFriendRequest.setState("ADDED");
        List<FriendRequest> list2 = new ArrayList<FriendRequest>();
        list2.add(mockFriendRequest);
        when(event.getAuthor())
                .thenReturn(user);
        when(user.getId())
                .thenReturn(DISCORD_ID);
        when(event.getArgs())
                .thenReturn(ME_ARGS);
        when(discordUserService.getDiscordUser(DISCORD_ID))
                .thenReturn(discordUser);
        when(friendRequestService.getFriendRequestByUser(discordUser))
                .thenReturn(list2);
        addFriendListCommand.execute(event);
    }

    @Test
    public void testAddFriendListByOtherUser() {
        List<FriendRequest> list = new ArrayList<>();
        list.add(mockFriendRequest);
        when(event.getAuthor())
                .thenReturn(user);
        when(user.getId())
                .thenReturn(DISCORD_ID);
        when(event.getArgs())
                .thenReturn(OTHER_ARGS);
        when(discordUserService.getDiscordUser(DISCORD_ID))
                .thenReturn(discordUser);
        when(friendRequestService.getFriendRequestByFriend(discordUser))
                .thenReturn(list);
        addFriendListCommand.execute(event);

        mockFriendRequest.setState("ADDED");
        List<FriendRequest> list2 = new ArrayList<FriendRequest>();
        list2.add(mockFriendRequest);
        when(event.getAuthor())
                .thenReturn(user);
        when(user.getId())
                .thenReturn(DISCORD_ID);
        when(event.getArgs())
                .thenReturn(OTHER_ARGS);
        when(discordUserService.getDiscordUser(DISCORD_ID))
                .thenReturn(discordUser);
        when(friendRequestService.getFriendRequestByFriend(discordUser))
                .thenReturn(list2);
        addFriendListCommand.execute(event);
    }

    @Test
    public void testAddFriendListInvalidArgument() {
        when(event.getAuthor())
                .thenReturn(user);
        when(user.getId())
                .thenReturn(DISCORD_ID);
        when(event.getArgs())
                .thenReturn("you");

        addFriendListCommand.execute(event);
    }
}

