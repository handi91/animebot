package com.jojonicho.animebot.friend.commands;

import static org.mockito.Mockito.when;

import com.jagrosh.jdautilities.command.CommandEvent;
import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.discorduser.service.DiscordUserServiceImpl;
import com.jojonicho.animebot.friend.model.FriendRequest;
import com.jojonicho.animebot.friend.service.FriendRequestService;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import com.jojonicho.animebot.friend.state.AddedState;
import com.jojonicho.animebot.friend.state.IdleState;
import com.jojonicho.animebot.friend.state.StateHelper;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class AddFriendCommandTest {
    @InjectMocks
    AddFriendCommand addFriendCommand;

    @Mock
    CommandEvent event;

    @Mock
    private DiscordUserServiceImpl discordUserService;

    @Mock
    private FriendRequestService friendRequestService;

    @Mock
    private StateHelper stateHelper;

    @Mock
    private IdleState idleState;

    @Mock
    private AddedState addedState;

    @Mock
    private Message message;

    @Mock
    private User user;

    @Mock
    private User friend;

    @Mock
    private Member member;

    private DiscordUser discordUser;
    private DiscordUser friendUser;
    private FriendRequest mockFriendRequest;
    private final String DISCORD_ID = "264258056416657419";
    private final String FRIEND_ID = "708209585407655966";

    @BeforeEach
    public void setUp() {
        discordUser = new DiscordUser();
        discordUser.setId(DISCORD_ID);

        friendUser = new DiscordUser();
        friendUser.setId(FRIEND_ID);

        mockFriendRequest = new FriendRequest();
        mockFriendRequest.setDiscordUser(discordUser);
        mockFriendRequest.setFriendUser(friendUser);
        mockFriendRequest.setUserName("dummy");
        mockFriendRequest.setFriendName("dummy2");
        mockFriendRequest.setState("IDLE");
    }

    @Test
    public void testSuccesfulAdded() {
        List<Member> list = new ArrayList<Member>();
        list.add(member);
        when(event.getAuthor())
                .thenReturn(user);
        when(user.getId())
                .thenReturn(DISCORD_ID);
        when(discordUserService.getDiscordUser(DISCORD_ID))
                .thenReturn(discordUser);
        when(event.getMessage())
                .thenReturn(message);
        when(message.getMentionedMembers())
                .thenReturn(list);
        when(member.getUser())
                .thenReturn(friend);
        when(friend.getId())
                .thenReturn(FRIEND_ID);
        when(discordUserService.getDiscordUser(FRIEND_ID))
                .thenReturn(friendUser);
        when(friendRequestService.createFriendRequest(discordUser, friendUser, user.getAsMention(), friend.getAsMention()))
                .thenReturn(mockFriendRequest);
        when(stateHelper.getRequestState(mockFriendRequest))
                .thenReturn(idleState);
        when(friend.getName())
                .thenReturn("Dummy");
        when(member.getTimeJoined())
                .thenReturn(OffsetDateTime.MIN);
        when(friend.getEffectiveAvatarUrl())
                .thenReturn("https://images-ext-2.discordapp.net/external/GyQicPLz_zQO15bOMtiGTtC4Kud7JjQbs1Ecuz7RrtU/https/cdn.discordapp.com/embed/avatars/1.png");
        addFriendCommand.execute(event);
    }

    @Test
    public void testAddSelf() {
        List<Member> list = new ArrayList<Member>();
        list.add(member);
        when(event.getAuthor())
                .thenReturn(user);
        when(user.getId())
                .thenReturn(DISCORD_ID);
        when(discordUserService.getDiscordUser(DISCORD_ID))
                .thenReturn(discordUser);
        when(event.getMessage())
                .thenReturn(message);
        when(message.getMentionedMembers())
                .thenReturn(list);
        when(member.getUser())
                .thenReturn(friend);
        when(friend.getId())
                .thenReturn(FRIEND_ID);
        when(discordUserService.getDiscordUser(FRIEND_ID))
                .thenReturn(discordUser);
        addFriendCommand.execute(event);
    }

    @Test
    public void testAddAlreadyExist() {
        List<Member> list = new ArrayList<Member>();
        list.add(member);
        when(event.getAuthor())
                .thenReturn(user);
        when(user.getId())
                .thenReturn(DISCORD_ID);
        when(discordUserService.getDiscordUser(DISCORD_ID))
                .thenReturn(discordUser);
        when(event.getMessage())
                .thenReturn(message);
        when(message.getMentionedMembers())
                .thenReturn(list);
        when(member.getUser())
                .thenReturn(friend);
        when(friend.getId())
                .thenReturn(FRIEND_ID);
        when(discordUserService.getDiscordUser(FRIEND_ID))
                .thenReturn(friendUser);
        when(friendRequestService.createFriendRequest(discordUser, friendUser, user.getAsMention(), friend.getAsMention()))
                .thenReturn(mockFriendRequest);
        when(stateHelper.getRequestState(mockFriendRequest))
                .thenReturn(addedState);
        when(friend.getName())
                .thenReturn("Dummy");
        when(member.getTimeJoined())
                .thenReturn(OffsetDateTime.MIN);
        when(friend.getEffectiveAvatarUrl())
                .thenReturn("https://images-ext-2.discordapp.net/external/GyQicPLz_zQO15bOMtiGTtC4Kud7JjQbs1Ecuz7RrtU/https/cdn.discordapp.com/embed/avatars/1.png");
        addFriendCommand.execute(event);
    }

    @Test
    public void testAddIllegalArgument() {
        List<Member> list = new ArrayList<Member>();
        list.add(member);

        when(event.getMessage())
                .thenReturn(message);
        when(message.getMentionedMembers())
                .thenThrow(new IndexOutOfBoundsException());

        addFriendCommand.execute(event);
    }
}
